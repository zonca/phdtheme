<?php
function get_current_page_depth($object){
    $parent_id  = $object->post_parent;
    $depth = 0;
    while($parent_id > 0){
        $page = get_page($parent_id);
        $parent_id = $page->post_parent;
        $depth++;
    }
 
    return $depth;
}
if (is_page( )) {

//    if (get_current_page_depth($post) == 1) {
//        $root_id = $post->id;
//
//    
//    $parent_id  = $post->post_parent;
//    $depth = 0;
//    while ($parent_id > 0) {
//        $page = get_page($parent_id);
//        $depth = get_current_page_depth(
//        $parent_id = $page->post_parent;
//        $depth++;
//    }

if (get_current_page_depth($post) > 0) {
    $root_id = $post->post_parent;
    $object = get_page($root_id);
    while (get_current_page_depth($object)>0) {
        $root_id = $object->post_parent;
        $object = get_page($root_id);
}
  $children = wp_list_pages("title_li=&child_of=".$root_id."&echo=0");
  $titlenamer = get_the_title($post->post_parent);
    $children = str_replace("current_page_item", "active", $children);
    $children = str_replace("children", "nav nav-list", $children);
}
}
  if ($children) { ?>

  <h2> <?php echo $titlenamer; ?> </h2>
  <ul class="nav nav-list">
  <?php echo $children; ?>
  </ul>

<?php } ?>
<?php dynamic_sidebar('sidebar-primary'); ?>
