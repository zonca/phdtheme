<?php

// Custom functions

add_action('roots_content_before', 'phd_breadcrumbs');

function phd_breadcrumbs() {
    if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('<p id="breadcrumbs">','</p>');
    }
}
