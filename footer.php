
  </div><!-- /#wrap -->

  <?php roots_footer_before(); ?>
  <div id="bottom-navbar-inner" class="navbar-inner">
      <div class="<?php echo WRAP_CLASSES; ?>">
<p><a href="http://www.fisica.unimi.it">Physics Department</a> | <a href="http://www.unimi.it">UNIVERSITÀ DEGLI STUDI DI MILANO</a> | <a href="http://goo.gl/maps/dNojP">Via Celoria, 16 - 20133 Milano - ITALY </a></p>
      <div id="green-bar"> <div id="darkgreen-bar" class="pull-right hidden-phone"> </div> </div> 
      </div>
  </div>

  <div id="bottom-unimi-bar">
  <footer id="content-info" class="<?php echo WRAP_CLASSES; ?>" role="contentinfo">
    <?php roots_footer_inside(); ?>
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p class="copy"><small><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-sa/3.0/80x15.png" /></a> | Page last updated: <?php the_modified_time('F jS, Y');?></small></p>
  </footer>
</div>
  <?php roots_footer_after(); ?>

  <?php wp_footer(); ?>
  <?php roots_footer(); ?>

</body>
</html>
