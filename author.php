<?php get_header(); ?>
  <?php roots_content_before(); ?>
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
    <?php roots_main_before(); ?>
      <div id="main" class="<?php echo MAIN_CLASSES; ?>" role="main">
        <div class="pad">
        <div class="page-header">
<?php
$curuser = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
?>
          <h1>
            <?php
      echo "$curuser->first_name $curuser->last_name";
?>
          </h1>
<p> </p>
<?php echo get_avatar( $curuser->ID ); ?>
<?php if ( is_user_logged_in() ) { ?>
<p><i>Notice to logged-in users</i>: You can add your picture by registering to <a href="http://gravatar.com">gravatar.com</a> with the same email address registered on this website.</p>
<?php } ?>

<table class="table table-hover">
        <tr><td>Phone:              </td><td><?php echo $curuser->phone; ?>   </td> </tr>
<?php if ( is_user_logged_in() ) { ?>
        <tr><td>Email (visible only to logged in users):              </td><td><?php echo $curuser->user_email; ?>   </td> </tr>
<?php } ?>
        <tr><td>Homepage:           </td><td><a href="<?php echo $curuser->user_url; ?>"><?php echo $curuser->user_url; ?></a></td> </tr>


<?php
    if (user_can( $curuser, 'edit_posts')) {
                global $post;
                $author_id = $post->post_author;

?>
        <?php roots_loop_before(); ?>
        <?php get_template_part('loop', 'category'); ?>
        <?php roots_loop_after(); ?>

<?php } else { ?>
        <tr><td>PhD School Cycle:   </td><td><?php echo $curuser->cycle; ?></td> </tr>
        <tr><td>Research Area:      </td><td><?php echo $curuser->researcharea; ?></td> </tr>
        <tr><td>Bio:                </td><td><?php echo nl2br($curuser->description); ?></td> </tr>
<?php } ?>

</table>
        </div>
      </div>
      </div><!-- /#main -->
    <?php roots_main_after(); ?>
    <?php roots_sidebar_before(); ?>
      <aside id="sidebar" class="<?php echo SIDEBAR_CLASSES; ?>" role="complementary">
      <?php roots_sidebar_inside_before(); ?>
        <?php get_sidebar(); ?>
      <?php roots_sidebar_inside_after(); ?>
      </aside><!-- /#sidebar -->
    <?php roots_sidebar_after(); ?>
    </div><!-- /#content -->
  <?php roots_content_after(); ?>
<?php get_footer(); ?>
