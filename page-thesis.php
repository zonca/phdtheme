<?php
/*
Template Name: Thesis
*/
get_header(); ?>
  <?php roots_content_before(); ?>
    <div id="content" class="<?php echo CONTAINER_CLASSES; ?>">
    <?php roots_main_before(); ?>
      <div id="main" class="<?php echo MAIN_CLASSES; ?>" role="main">
        <?php roots_loop_before(); ?>

        <?php get_template_part('loop', 'page'); ?>
        <?php roots_loop_after(); ?>
<?php 
global $wp_roles;
//print_r($wp_roles->role_names);
$role = "subscriber";
$this_role = "'[[:<:]]".$role."[[:>:]]'";

for ($i = date('Y'); $i >= 2007; $i--) {

$query = "SELECT * FROM $wpdb->users WHERE ID = ANY (SELECT user_id FROM $wpdb->usermeta WHERE (meta_key = 'thesisyear' AND meta_value = $i)) ORDER BY user_nicename ASC LIMIT 10000";
$users_of_this_role = $wpdb->get_results($query);

//print_r($users_of_this_role);
if ($users_of_this_role) {
    echo "<h2>$i</h2>";
    echo '<ul>';
    foreach($users_of_this_role as $user) {
      $curuser = get_userdata($user->ID);
      echo '<li>'. $curuser->display_name . ": <a href=\"$curuser->thesislink\">$curuser->thesistitle</a></li>";
    }
    echo '</ul>';
}
}


?>
      </div><!-- /#main -->
    <?php roots_main_after(); ?>
    <?php roots_sidebar_before(); ?>
      <aside id="sidebar" class="<?php echo SIDEBAR_CLASSES; ?>" role="complementary">
      <?php roots_sidebar_inside_before(); ?>
        <?php get_sidebar(); ?>
      <?php roots_sidebar_inside_after(); ?>
      </aside><!-- /#sidebar -->
    <?php roots_sidebar_after(); ?>
    </div><!-- /#content -->
  <?php roots_content_after(); ?>
<?php get_footer(); ?>
